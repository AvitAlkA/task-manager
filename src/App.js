import React from 'react'

import TaskList from './TaskList'
import Layout from './Layout'

const App = () => {
  return (
    <Layout>
      <TaskList header="Task List Manager" />
    </Layout>
  )
}

export default App
