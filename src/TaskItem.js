import React, { Component } from 'react'
import PropTypes from 'prop-types'

const itemStyle = {
  display: 'flex',
  alignItems: 'center',
  paddingTop: 5,
}

export default class TaskItem extends Component {
  constructor(props) {
    super(props)

    this.state = {
      editedTaskName: props.task.title,
    }
  }

  render() {
    const {
      task,
      onToggle,
      onDelete,
      isEdited,
      onToggleEdit,
      onEdit,
      onTogglePinTask,
    } = this.props
    return (
      <li style={itemStyle}>
        <button onClick={() => {onTogglePinTask(task.id)}}>    
          <span role="img" aria-label="Pin Task"> 
            &#128206;
          </span>
        </button>
        <input
          type="checkbox"
          checked={task.done}
          onChange={() => onToggle(task.id)}
        />
        {isEdited ? (
          <input
            type="text"
            value={this.state.editedTaskName}
            onChange={e => this.setState({ editedTaskName: e.target.value })}
            onKeyPress={e => {
              if (e.key === 'Enter') {
                onEdit(this.state.editedTaskName)
              }
            }}
          />
        ) : (
          <span
            onDoubleClick={() => onToggleEdit(task.id)}
            style={{ flexGrow: 1 }}
          >
            {task.title}
          </span>
        )}
        <button onClick={() => onDelete(task.id)}>✖</button>
        <button
          onClick={() => {
            if (isEdited) {
              onEdit(this.state.editedTaskName)
              onToggleEdit(null)
            } else {
              onToggleEdit(task.id)
            }
          }}
        >
          <span role="img" aria-label="Edit">
            &#x270D;
          </span>
        </button>
      </li>
    )
  }
}

TaskItem.propTypes = {
  task: PropTypes.object.isRequired,
  onToggle: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  isEdited: PropTypes.bool.isRequired,
  onToggleEdit: PropTypes.func.isRequired,
  onEdit: PropTypes.func.isRequired,
  onTogglePinTask: PropTypes.func.isRequired
}
