import React from 'react'
import PropTypes from 'prop-types'

const CreateTask = props => {
  const { onCreate, newTaskName, onChange } = props
  return (
    <div>
      <input
        value={newTaskName}
        onChange={e => onChange(e.target.value)}
        type="text"
        placeholder="New task"
        onKeyPress={e => {
          if (e.key === 'Enter') {
            onCreate(newTaskName)
          }
        }}
      />
      <button
        onClick={() => {
          onCreate(newTaskName)
        }}
      >
        Add
      </button>
    </div>
  )
}
//check
CreateTask.propTypes = {
  onCreate: PropTypes.func.isRequired,
  newTaskName: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
}

export default CreateTask
