import React from 'react'
import PropTypes from 'prop-types'

const mainDivStyle = {
  border: '3px solid #2ecc71',
  borderRadius: 20,
  padding: 20
}

const Layout = props =>{ 
  const { children } = props
  return(
<div style={mainDivStyle}>{children}</div>)
}


export default Layout

Layout.propTypes = {
  children: PropTypes.any.isRequired,
}


