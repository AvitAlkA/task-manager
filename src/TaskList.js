import React from 'react'

import TaskItem from './TaskItem'
import CreateTask from './CreateTask'
import FuzzySearch from 'fuzzy-search'

const taskListStyle = {
  paddingLeft: 0,
  margin: 0,
  marginTop: 15
}

export default class TaskList extends React.Component {
  state = {
    tasks: [],
    editedTaskId: null,
    newTaskName: '',
    searchTerm: '',
    sortType: '',
    pinnedTasks: [],
    pinnedTasksId: [],

    error: null,
    isLoaded: false
  }

  componentDidMount() {
    fetch('http://localhost:3002/tasks')
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            isLoaded: true,
            tasks: result
          })
        },
        error => {
          this.setState({
            isLoaded: true,
            error
          })
        }
      )
  }

  toggleTask = taskId => {
    fetch('http://localhost:3002/tasks', {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ id: taskId, newTitle: '' })
    }).then(response => response.json)

    this.setState(prevState => {
      const newTasks = []

      for (const task of prevState.tasks) {
        newTasks.push({
          ...task,
          done: task.id === taskId ? !task.done : task.done
        })
      }
      return { tasks: newTasks }
    })
  }

  deleteTask = taskId => {
    fetch('http://localhost:3002/tasks', {
      method: 'DELETE',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ id: taskId })
    }).then(response => response.json)

    this.setState(prevState => {
      const newTasks = prevState.tasks.filter(task => taskId !== task.id)
      return { tasks: newTasks }
    })
  }

  addTask = taskName => {
    taskName = taskName.trim()
    if (!taskName) return

    const prevState = [...this.state.tasks]
    const maxId = Math.max(0, ...this.state.tasks.map(task => task.id))
    const newTaskMade = {
      id: maxId + 1,
      title: taskName,
      date: new Date(),
      done: false
    }

    fetch('http://localhost:3002/tasks', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(newTaskMade)
    })
      .then(res => res.json())
      .then(() => {
        this.setState({ tasks: [...prevState, newTaskMade], newTaskName: '' })
      })
  }

  onChange = value => {
    this.setState({ newTaskName: value })
  }

  setEditedTask = editedTaskId => {
    this.setState({ editedTaskId })
  }

  editTaskTitle = newTitle => {
    fetch('http://localhost:3002/tasks', {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        id: this.state.editedTaskId,
        newTitle: newTitle
      })
    }).then(response => response.json)
    this.setState(prevState => {
      const index = prevState.tasks.findIndex(
        task => task.id === prevState.editedTaskId
      )
      const newTasks = [...prevState.tasks]
      newTasks.splice(index, 1, {
        ...prevState.tasks[index],
        title: newTitle
      })
      return { tasks: newTasks, editedTaskId: null }
    })
  }

  searchByName() {
    const searcher = new FuzzySearch(this.state.tasks, ['title'])
    return searcher.search(this.state.searchTerm)
  }

  sortTasks(sortType) {
    if (sortType === '') {
      this.state.tasks.sort((a, b) => {
        return a.id - b.id
      })
    } else if (sortType === 'name') {
      this.state.tasks.sort((a, b) => {
        var nameA = a.title.toUpperCase()
        var nameB = b.title.toUpperCase()
        if (nameA < nameB) {
          return -1
        } else if (nameA > nameB) {
          return 1
        }
        return 0
      })
    } else if (sortType === 'date') {
      this.state.tasks.sort((a, b) => {
        return new Date(a.date) - new Date(b.date)
      })
    }
  }

  pinTask = taskId => {
    let pinned = [...this.state.pinnedTasks]
    let pinnedId = [...this.state.pinnedTasksId]
    if (!pinnedId.includes(taskId)) {
      pinned.push(this.state.tasks[taskId - 1])
      pinnedId.push(taskId)
    } else {
      let deleteP = pinnedId.indexOf(taskId)
      pinnedId.splice(deleteP, 1)
      for (let i = 0; i < pinned.length; i++) {
        if (pinned[i] === taskId) deleteP = i
      }
      pinned.splice(deleteP, 1)
    }
    this.setState({ pinnedTasks: [...pinned], pinnedTasksId: [...pinnedId] })
  }

  render() {
    const numOfDoneTasks = this.state.tasks.filter(task => task.done).length
    if (this.state.error) {
      return <div>Error: {this.state.error.message}</div>
    } else if (!this.state.isLoaded) {
      return <div>Loading...</div>
    } else {
      return (
        <React.Fragment>
          <CreateTask
            onCreate={this.addTask}
            onChange={this.onChange}
            newTaskName={this.state.newTaskName}
          />
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-around',
              marginTop: 15
            }}
          >
            <span>Not Done: {this.state.tasks.length - numOfDoneTasks}</span>
            <span>Done: {numOfDoneTasks}</span>
          </div>
          <ul style={taskListStyle} />
          {this.state.pinnedTasks.map(task => (
            <TaskItem
              key={task.id}
              task={task}
              onToggle={this.toggleTask}
              onDelete={this.deleteTask}
              isEdited={this.state.editedTaskId === task.id}
              onToggleEdit={this.setEditedTask}
              onEdit={this.editTaskTitle}
              onTogglePinTask={this.pinTask}
            />
          ))}
          <div>
            <input
              type="text"
              placeholder="Search"
              value={this.state.searchTerm}
              onChange={e => {
                this.setState({ searchTerm: e.target.value })
              }}
            />
          </div>
          <form>
            <select
              name="sortTasks"
              value={this.state.sortType}
              onChange={e => {
                this.setState({ sortType: e.target.value })
                this.sortTasks(e.target.value)
              }}
            >
              <option value="">Choose an option</option>
              <option value="name">Name</option>
              <option value="date">Date</option>
            </select>
          </form>
          <ul style={taskListStyle}>
            {this.state.searchTerm
              ? this.searchByName().map(task => {
                  if (!this.state.pinnedTasksId.includes(task.id)) {
                    return (
                      <TaskItem
                        key={task.id}
                        task={task}
                        onToggle={this.toggleTask}
                        onDelete={this.deleteTask}
                        isEdited={this.state.editedTaskId === task.id}
                        onToggleEdit={this.setEditedTask}
                        onEdit={this.editTaskTitle}
                        onTogglePinTask={this.pinTask}
                      />
                    )
                  } else return
                })
              : this.state.tasks.map(task => {
                  if (!this.state.pinnedTasksId.includes(task.id)) {
                    return (
                      <TaskItem
                        key={task.id}
                        task={task}
                        onToggle={this.toggleTask}
                        onDelete={this.deleteTask}
                        isEdited={this.state.editedTaskId === task.id}
                        onToggleEdit={this.setEditedTask}
                        onEdit={this.editTaskTitle}
                        onTogglePinTask={this.pinTask}
                      />
                    )
                  } else return
                })}
          </ul>
        </React.Fragment>
      )
    }
  }
}
